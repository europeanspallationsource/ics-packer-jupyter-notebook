Packer template for the ESS Jupyter Notebook Docker image
=========================================================

WARNING! THIS REPOSITORY IS DEPRECATED.

Please check https://bitbucket.org/europeanspallationsource/ics-docker-jupyter-notebook

-----------------------------------------------------------------------------

This Packer_ template creates a Docker_ image with the ESS Jupyter Notebook.

The Notebook includes:
    - Python 3
    - Python 2
    - Julia
    - OpenXAL
    - Mantid

Is is inspired by [jupyter docker-stacks](https://github.com/jupyter/docker-stacks).
Provisionning is done using Ansible_ playbooks. It is split in several templates to create several layers.
Doing everything in the same template results in a layer too big to be pushed to the Docker hub.


Usage
-----

To be able to push the image to Docker Hub, you first have to login::

    $ docker login

You can then run::

    $ ./build.sh

This will download the required Ansible_ roles and create and push the image `europeanspallationsource/notebook`.


.. _Packer: https://www.packer.io
.. _Docker: https://www.docker.com
.. _Ansible: http://docs.ansible.com/ansible/
