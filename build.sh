#!/bin/bash

# Exit on any error
set -eux

ansible-galaxy install --force -r requirements.yml -p roles/

packer build jupyter-notebook-01.json
packer build jupyter-notebook-02.json
packer build jupyter-notebook-03.json
packer build jupyter-notebook-04.json
packer build jupyter-notebook-05.json
packer build jupyter-notebook-06.json
